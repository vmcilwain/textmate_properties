<h2>Textmate2 Properties</h2>

This is a theme from Hilton Lipschitze which I found it to be pretty useful as a starter configuration for Textmate2. I did change the font size which was originally set at 12.

To use this file clone the repository to your system and copy the file to your home folder as a hidden file.

`cp tm_properties ~/.tm_properties`

Then restart Textmate2

That should be all.